#! /usr/bin/env python

from argparse import ArgumentParser
from ast import literal_eval
from json import dumps
from sys import exit
import os
import re
import shutil
import zipfile

p = ArgumentParser(description="""Compresses each mod in the current folder \
                            into a format distributable by Technic Solder.""")
p.add_argument('--keep', action='store_true',
               help="specifies that mods should not be deleted afterwards")
p.add_argument('--overwrite', action='store_true',
               help="replaces file if naming conflicts occur")
p.add_argument('--verbose', '-v', action='store_true',
               help="increase verbosity")
p.add_argument('--certain', action='store_true',
               help="do not guess modname from name of input file")
p.add_argument('--fill', action='store', metavar='DICT',
               help="fill in the blanks, ex {'mcversion': '1.10'}")
p.add_argument('--json', action='store_true',
               help="creates a .info file in the folder with useful info")
args = p.parse_args()

guess = re.compile("(\w{3}[\w\-\:]*\w+)")
strip = re.compile("[^\d\w\.\-\_]")
space = re.compile("\s+")

d = {}
if args.fill:
    d = literal_eval(args.fill)


def search(item, location):
    match = re.search('"{}"\s*:\s*"([\w\d\-\s\:\'\.\#\;\_]+)"'.format(item),
                      location)
    return match


def zip_name(info, f):
    items = ["name", "mcversion", "version"]
    r = []

    for item in items:
        match = search(item, info)

        if not match and item == 'name':
            match = guess.search(f)

        if match:
            match = space.sub("-", match.group(1).lower())
            match = strip.sub("", match)
        elif item in d:
            match = d[item]
        else:
            match = item.upper()
            if r[0] != 'XXX':
                r.insert(0, 'XXX')
        r.append(match)

    return r


def zip_files(info, f):
    zipname = "-".join(info) + '.zip'
    modname = info[-3]

    with zipfile.ZipFile(zipname, mode="w") as z:
        z.write(f, os.path.join('mods', modname), zipfile.ZIP_DEFLATED)
        z.close()

    if not os.path.exists(modname):
        os.makedirs(modname)

    if 'XXX-' in zipname:
        print("WARNING:  Incomplete name for {}".format(zipname))
    else:
        try:
            shutil.move(zipname, modname)
        except shutil.Error as e:
            if args.overwrite:
                shutil.copy(zipname, modname)
                os.remove(zipname)
            else:
                print("ERROR:", e)
                exit(1)

        if args.verbose:
            print("OK:      ", f, '-->', os.path.join(modname, zipname))

    if not args.keep:
        os.remove(f)

    return modname


def make_json(info, f, folder):
    items = ["name", "authors", "url", "description"]
    r = {}

    for item in items:
        match = search(item, info)
        if match:
            r[item] = match.group(1)
        else:
            r[item] = 'NULL'

    if f.endswith(".jar") or f.endswith(".zip"):
        filename = f[:-4] + '.info'

    info_path = os.path.join(folder, filename)
    with open(info_path, "w") as info_file:
        info_file.write(dumps(r, sort_keys=True, indent=4))
        info_file.close()

        if args.verbose:
            print("          Wrote info -->", info_path)
    return


def main():
    files = [f for f in os.listdir('.') if os.path.isfile(f)]
    for f in sorted(files):
        if zipfile.is_zipfile(f) and 'XXX-' not in f:
            with zipfile.ZipFile(f, mode="r") as z:
                if 'mcmod.info' in z.namelist():
                    info = bytes.decode(z.open('mcmod.info').read())

                    if info or not args.certain:
                        zipname = zip_name(info, f)
                        folder = zip_files(zipname, f)
                    else:
                        print("SKIPPING: No mcmod.info file in {}".format(f))

                    if args.json and info:
                        make_json(info, f, folder)

                    z.close()


if __name__ == '__main__':
    main()
