#!/usr/bin/env python
# compress-configs.py
# Compresses the files in the config folder into a format distributable using
# Technic Solder.

import os
import zipfile

mcVersion = "1.7.10"
zipname = "echoreply-configs-" + mcVersion + "-VERSION.zip"

with zipfile.ZipFile(zipname, mode='w') as z:
    files = [f for f in os.listdir('.') if os.path.isfile(f)]
    for f in files:
        z.write(f, os.path.join('config', f), zipfile.ZIP_DEFLATED)
